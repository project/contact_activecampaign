<?php

/**
 * @file
 * Hooks provided by the contact_activecampaign module.
 */

use Drupal\Core\Field\FieldItemListInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters the value sent to ActiveCampaign.
 *
 * @param mixed $value
 *   The value.
 * @param \Drupal\Core\Field\FieldItemListInterface $field
 *   The field this value is.
 * @param string[] $activecampaign_property
 *   The AC entity this value is mapped to. It contains a group and a property
 *   key.
 */
function hook_contact_activecampaign_mapped_field_value_alter(&$value, FieldItemListInterface $field, array $activecampaign_property) {
  // Modify $value here.
}

/**
 * Modifies the allowed base fields.
 *
 * See ContactFormSettingsform.
 *
 * @param array $allowed_base_fields
 *   The allowed base fields.
 */
function hook_contact_activecampaign_allowed_base_fields_alter(array &$allowed_base_fields) {
  // Modify $allowed_base_fields here.
}

/**
 * Modify the label returned by the entity reference field mapper.
 *
 * @param string $label
 *   The label.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The related entity.
 */
function hook_contact_activecampaign_entity_reference_field_mapper_label_alter(string $label, \Drupal\Core\Entity\EntityInterface $entity) {
  // Modify the entity label.
}

/**
 * @} End of "addtogroup hooks".
 */
