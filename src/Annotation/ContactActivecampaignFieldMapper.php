<?php

namespace Drupal\contact_activecampaign\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class FieldMapperPlugin.
 *
 * Defines a FieldMapperPlugin annotation object.
 *
 * @package Drupal\contact_activecampaign\Annotation
 *
 * @Annotation
 */
class ContactActivecampaignFieldMapper extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin title.
   *
   * @var string
   */
  public $title;

}
