<?php

namespace Drupal\contact_activecampaign\Plugin\ContactActivecampaignFieldMapper;

use Drupal\contact_activecampaign\FieldMapperPluginBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultFieldMapper.
 *
 * The default field mapper plugin.
 *
 * @ContactActivecampaignFieldMapper(
 *   id = "entity_reference",
 *   title = @Translation("Entity reference field mapper")
 * )
 *
 * @package Drupal\contact_activecampaign\Plugin\ContactActivecampaignFieldMapper
 */
class EntityReferenceFieldMapper extends FieldMapperPluginBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMappableTypes(): array {
    return [
      'entity_reference',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldValue(FieldItemListInterface $fieldItemList, int $delta, string $column): string {
    /**
     * @var \Drupal\Core\Field\EntityReferenceFieldItemList $fieldItemList
     */
    $referenced_entities = $fieldItemList->referencedEntities();

    $label = isset($referenced_entities[$delta]) ? $referenced_entities[$delta]->label() : '';

    if (isset($referenced_entities[$delta])) {
      $this->moduleHandler->alter('entity_reference_field_mapper_label', $label, $referenced_entities[$delta]);
    }
    return $label;
  }

}
