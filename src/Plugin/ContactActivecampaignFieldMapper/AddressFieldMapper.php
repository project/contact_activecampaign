<?php

namespace Drupal\contact_activecampaign\Plugin\ContactActivecampaignFieldMapper;

use Drupal\contact_activecampaign\FieldMapperPluginBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AddressFieldMapper.
 *
 * The Address field type mapper plugin.
 *
 * @ContactActivecampaignFieldMapper(
 *   id = "address",
 *   title = @Translation("Address field mapper")
 * )
 *
 * @package Drupal\contact_activecampaign\Plugin\ContactActivecampaignFieldMapper
 */
class AddressFieldMapper extends FieldMapperPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMappableTypes(): array {
    if ($this->moduleHandler->moduleExists('address')) {
      return [
        'address',
      ];
    }
    return [];
  }

}
