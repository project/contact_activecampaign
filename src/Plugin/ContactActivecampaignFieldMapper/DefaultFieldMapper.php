<?php

namespace Drupal\contact_activecampaign\Plugin\ContactActivecampaignFieldMapper;

use Drupal\contact_activecampaign\FieldMapperPluginBase;

/**
 * Class DefaultFieldMapper.
 *
 * The default field mapper plugin.
 *
 * @ContactActivecampaignFieldMapper(
 *   id = "default",
 *   title = @Translation("Default field mapper")
 * )
 *
 * @package Drupal\contact_activecampaign\Plugin\ContactActivecampaignFieldMapper
 */
class DefaultFieldMapper extends FieldMapperPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getMappableTypes(): array {
    return [
      'integer',
      'string',
      'email',
      'string_long',
      'text_long',
      'list_string',
      'list_integer',
      'boolean',
      'datetime',
      'telephone',
      'phone_international',
    ];
  }

}
