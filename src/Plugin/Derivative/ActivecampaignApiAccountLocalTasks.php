<?php

namespace Drupal\contact_activecampaign\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Generate local tasks for all activecampaign api account entities.
 */
class ActivecampaignApiAccountLocalTasks extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $storage = \Drupal::entityTypeManager()
      ->getStorage('activecampaign_api_account');

    /**
     * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface[] $activecampaign_api_accounts
     */
    $activecampaign_api_accounts = $storage->loadMultiple();
    foreach ($activecampaign_api_accounts as $activecampaign_api_account) {
      $this->derivatives['entity.contact_form.contact_activecampaign_api_account.' . $activecampaign_api_account->id()] = $base_plugin_definition;
      $this->derivatives['entity.contact_form.contact_activecampaign_api_account.' . $activecampaign_api_account->id()]['title'] = $activecampaign_api_account->label();
      $this->derivatives['entity.contact_form.contact_activecampaign_api_account.' . $activecampaign_api_account->id()]['route_name'] = 'entity.contact_form.contact_activecampaign_api_account';
      $this->derivatives['entity.contact_form.contact_activecampaign_api_account.' . $activecampaign_api_account->id()]['parent_id'] = 'entity.contact_form.contact_activecampaign';
      $this->derivatives['entity.contact_form.contact_activecampaign_api_account.' . $activecampaign_api_account->id()]['route_parameters']['activecampaign_api_account'] = $activecampaign_api_account->id();
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
