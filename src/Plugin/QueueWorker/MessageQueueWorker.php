<?php

namespace Drupal\contact_activecampaign\Plugin\QueueWorker;

use Drupal\activecampaign_api\Exception as ActivecampaignApiException;
use Drupal\contact_activecampaign\Service\ContactFormManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MessageQueueWorker.
 *
 * The worker for the contact_activecampaign_message queue.
 *
 * @QueueWorker(
 *   id = "contact_activecampaign_message",
 *   title = @Translation("Queue worker for contact_activecampaign_message"),
 *   cron = {"time" = 30}
 * )
 *
 * @package Drupal\contact_activecampaign\Plugin\QueueWorker
 */
class MessageQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Contact message storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $messageStorage;

  /**
   * The contact form manager.
   *
   * @var \Drupal\contact_activecampaign\Service\ContactFormManagerInterface
   */
  protected $contactFormManager;

  /**
   * Activecampaign api account storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $activecampaignApiAccountStorage;

  /**
   * Constructs a new object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\contact_activecampaign\Service\ContactFormManagerInterface $contactFormManager
   *   The contact form manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $loggerChannelFactory, EntityTypeManagerInterface $entityTypeManager, ContactFormManagerInterface $contactFormManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $loggerChannelFactory->get('contact_activecampaign');
    $this->messageStorage = $entityTypeManager->getStorage('contact_message');
    $this->activecampaignApiAccountStorage = $entityTypeManager->getStorage('activecampaign_api_account');
    $this->contactFormManager = $contactFormManager;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return \Drupal\contact_activecampaign\Plugin\QueueWorker\MessageQueueWorker
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('contact_activecampaign.contact_form_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /**
     * @var \Drupal\contact\MessageInterface|null $message
     */
    $message = $this->messageStorage->load($data['contact_message_id']);
    if ($message === NULL) {
      $this->logger->error('Could not load contact_message @id', ['@id' => $data['contact_message_id']]);
      return;
    }

    /**
     * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface|null $activecampaign_api_account
     */
    $activecampaign_api_account = $this->activecampaignApiAccountStorage->load($data['activecampaign_api_account_id']);
    if ($activecampaign_api_account === NULL) {
      $this->logger->error('Could not load activecampaign_api_account for message @id', ['@id' => $data['activecampaign_api_account_id']]);
      return;
    }

    try {
      $this->contactFormManager->sendToActiveCampaign($message, $activecampaign_api_account);
    }
    catch (ActivecampaignApiException $e) {
      if ($e->getResponse()->getStatusCode() == 503) {
        // ActiveCampaign is temporarily unavailable. Try again later.
        throw new SuspendQueueException($e->getMessage());
      }
      if (!$e->isReported()) {
        // Exception not reported. Re-throw.
        watchdog_exception('contact_activecampaign', $e);
        throw $e;
      }
    }
    catch (\Exception $e) {
      watchdog_exception('contact_activecampaign', $e);
      throw $e;
    }
  }

}
