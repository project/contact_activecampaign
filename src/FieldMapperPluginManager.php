<?php

namespace Drupal\contact_activecampaign;

use Drupal\contact_activecampaign\Annotation\ContactActivecampaignFieldMapper;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class FieldMapperPluginManager.
 *
 * Field mapper plugin manager.
 *
 * @package Drupal\contact_activecampaign
 */
class FieldMapperPluginManager extends DefaultPluginManager {

  /**
   * FieldMapperPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    $this->subdir = 'Plugin/ContactActivecampaignFieldMapper';
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = ContactActivecampaignFieldMapper::class;
    $this->pluginInterface = FieldMapperPluginInterface::class;
    $this->moduleHandler = $module_handler;
  }

}
