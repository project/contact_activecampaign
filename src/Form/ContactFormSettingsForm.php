<?php

namespace Drupal\contact_activecampaign\Form;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\Endpoint\ContactLists;
use Drupal\activecampaign_api\Endpoint\Tags;
use Drupal\activecampaign_api\Service\EndpointFactoryInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\contact\ContactFormInterface;
use Drupal\contact\Entity\ContactForm;
use Drupal\contact_activecampaign\ListSubscriptionSetting;
use Drupal\contact_activecampaign\Service\ContactFormManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ContactFormSettingsForm.
 *
 * Manage the ActiveCampaign settings for the given form.
 *
 * @package Drupal\contact_activecampaign\Form
 */
class ContactFormSettingsForm extends FormBase {

  use StringTranslationTrait;

  /**
   * The contact form.
   *
   * @var \Drupal\contact\Entity\ContactForm
   */
  protected $contactForm;

  /**
   * The activecampaign api account.
   *
   * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface
   */
  protected $activeCampaignApiAccount;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Contact form manager.
   *
   * @var \Drupal\contact_activecampaign\Service\ContactFormManagerInterface
   */
  protected $contactFormManager;

  /**
   * The AC endpoint factory.
   *
   * @var \Drupal\activecampaign_api\Service\EndpointFactoryInterface
   */
  protected $endpointFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ContactFormSettingsForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   Entity display repository.
   * @param \Drupal\contact_activecampaign\Service\ContactFormManagerInterface $contactFormManager
   *   Contact form manager.
   * @param \Drupal\activecampaign_api\Service\EndpointFactoryInterface $endpointFactory
   *   The AC endpoint factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager, EntityDisplayRepositoryInterface $entityDisplayRepository, ContactFormManagerInterface $contactFormManager, EndpointFactoryInterface $endpointFactory, ModuleHandlerInterface $moduleHandler) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->contactFormManager = $contactFormManager;
    $this->endpointFactory = $endpointFactory;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Instantiates a new instance of this class.
   *
   * This is a factory method that returns a new instance of this class. The
   * factory should pass any needed dependencies into the constructor of this
   * class, but not the container itself. Every call to this method must return
   * a new instance of this class; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   *
   * @return \Drupal\contact_activecampaign\Form\ContactFormSettingsForm
   *   The new instance.
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository'),
      $container->get('contact_activecampaign.contact_form_manager'),
      $container->get('activecampaign_api.endpoint_factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_activecampaign_contact_form_settings';
  }

  /**
   * Title callback.
   *
   * @param \Drupal\contact\Entity\ContactForm|null $contact_form
   *   The form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface|null $activecampaign_api_account
   *   The activecampaign api account.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function title(ContactForm $contact_form = NULL, ActivecampaignApiAccountInterface $activecampaign_api_account = NULL) {
    return $this->t('Send submitted forms of <em>@form</em> to ActiveCampaign <em>@account</em>', [
      '@form' => $contact_form->label(),
      '@account' => $activecampaign_api_account->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ContactForm $contact_form = NULL, ActivecampaignApiAccountInterface $activecampaign_api_account = NULL) {

    if ($contact_form === NULL || $activecampaign_api_account === NULL) {
      throw new NotFoundHttpException();
    }

    $this->contactForm = $contact_form;
    $this->activeCampaignApiAccount = $activecampaign_api_account;
    $form['#contact_form'] = $this->contactForm;
    $form['#activecampaign_api_account'] = $this->activeCampaignApiAccount;

    $this->endpointFactory->setActivecampaignApiAccount($this->activeCampaignApiAccount);

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings for <em>@contact_form</em>', ['@contact_form' => $this->contactForm->label()]),
    ];

    $form['settings']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->contactFormManager->isEnabled($this->contactForm, $this->activeCampaignApiAccount),
    ];

    $send_when_options = $this->getSendWhenOptions($contact_form);

    $form['settings']['send_when'] = [
      '#type' => 'select',
      '#title' => $this->t('When to send to ActiveCampaign'),
      '#default_value' => $this->contactFormManager->getSendWhen($this->contactForm, $this->activeCampaignApiAccount),
      '#options' => $send_when_options,
    ];

    $form['settings']['contact'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('ActiveCampaign contact'),
    ];

    $form['settings']['contact']['determine_ac_contact_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Method to determine the current contact in ActiveCampaign'),
      '#options' => [
        ContactFormManagerInterface::DETERMINE_AC_CONTACT_METHOD_EMAIL => $this->t('Determine using the entered e-mail address'),
      ],
      '#default_value' => $this->contactFormManager->getDetermineActiveCampaignContactMethod($this->contactForm, $this->activeCampaignApiAccount),
    ];

    $form['settings']['contact']['create_nonexisting_contact'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create when non-existing'),
      '#description' => $this->t('When enabled, a new AC contact is created when the current contact could not be determined.'),
      '#default_value' => $this->contactFormManager->getCreateContactWhenNonExisting($this->contactForm, $this->activeCampaignApiAccount),
    ];

    if ($this->contactFormManager->supportsAccounts($this->activeCampaignApiAccount)) {
      $form['settings']['account'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('ActiveCampaign account'),
        '#description' => $this->t('An ActiveCampaign account will only be added/updated when one or more fields are linked to specific account fields.'),
      ];

      $form['settings']['account']['determine_ac_account_method'] = [
        '#type' => 'select',
        '#title' => $this->t('Method to determine the current account in ActiveCampaign'),
        '#options' => [
          ContactFormManagerInterface::DETERMINE_AC_ACCOUNT_METHOD_NAME => $this->t('Determine using the entered account name'),
        ],
        '#default_value' => $this->contactFormManager->getDetermineActiveCampaignAccountMethod($this->contactForm, $this->activeCampaignApiAccount),
      ];

      $form['settings']['account']['create_nonexisting_account'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create when non-existing'),
        '#description' => $this->t('When enabled, a new AC account is created when the current account could not be determined.'),
        '#default_value' => $this->contactFormManager->getCreateAccountWhenNonExisting($this->contactForm, $this->activeCampaignApiAccount),
      ];
    }
    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    $form['field_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields'),
      '#attributes' => [
        'id' => 'field-mapping',
      ],
    ];

    $mappable_field_types = $this->contactFormManager->getMappableFieldTypes();

    $allowed_base_fields = [
      'name',
      'mail',
      'subject',
      'message',
    ];

    $this->moduleHandler->alter('contact_activecampaign_allowed_base_fields', $allowed_base_fields);

    $field_mapping = $this->contactFormManager->getFieldMapping($this->contactForm, $this->activeCampaignApiAccount);
    /**
     * @var object[] $fields
     */
    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $contact_form->id());
    $form_display = $this->entityDisplayRepository->getFormDisplay('contact_message', $contact_form->id());
    $form_display_fields = $form_display->getComponents();

    foreach ($form_display->getComponents() as $field_name => $component) {
      if (!isset($fields[$field_name])) {
        continue;
      }
      $fields[$field_name]->weight = $component['weight'];
    }
    uasort($fields, [SortArray::class, 'sortByWeightProperty']);

    $visible_form_field_weight = 0;

    $form['field_mapping']['invisible_form_fields_header'] = [
      '#type' => 'markup',
      '#markup' => '<br><br><h3>' . $this->t('The following fields are not visible in the form:') . '</h3>',
      '#weight' => 1000,
    ];

    $field_mapping_options = $this->contactFormManager->getMappableFieldOptions($this->activeCampaignApiAccount);

    $checked_options = [];

    $boolean_field_types = [
      'boolean',
    ];

    foreach ($fields as $field_name => $form_display_field) {
      if (!isset($fields[$field_name])) {
        continue;
      }
      $field = $fields[$field_name];

      if (get_class($field) != FieldConfig::class && !in_array($field_name, $allowed_base_fields)) {
        continue;
      }

      if (in_array($field->getType(), $boolean_field_types)) {
        $checked_options[$field_name] = $this->t('<em>@field</em> is checked.', ['@field' => strip_tags($field->getLabel())]);
      }

      if (!in_array($field->getType(), $mappable_field_types)) {
        continue;
      }

      $is_visible_in_form = isset($form_display_fields[$field_name]);

      $columns = array_keys($field->getFieldStorageDefinition()
        ->getSchema()['columns']);

      foreach ($columns as $column) {
        $form['field_mapping'][$field_name . ':' . $column] = [
          '#type' => 'select',
          '#title' => $field->getLabel() . (count($columns) > 1 ? ': ' . $column : '') . ' (' . $field_name . ')',
          '#options' => $field_mapping_options,
          '#default_value' => $field_mapping[$field_name . ':' . $column] ?? '',
          '#weight' => $visible_form_field_weight + ($is_visible_in_form ? 0 : 1000),
        ];
      }

      $visible_form_field_weight++;
    }

    /**
     * @var \Drupal\activecampaign_api\Endpoint\ContactLists $contact_lists_endpoint
     */
    $contact_lists_endpoint = $this->endpointFactory->get(ContactLists::class);

    $contact_lists = [];
    try {
      $contact_lists = $contact_lists_endpoint->list([]);
    }
    catch (\Exception $e) {
      watchdog_exception('contact_activecampaign', $e);
      $this->messenger()->addError($e->getMessage());
    }

    foreach ($contact_lists as $contact_list) {
      $list_subscription_setting = $this->contactFormManager->getListSubscriptionSetting($this->contactForm, $this->activeCampaignApiAccount, $contact_list);

      $form['group_contact_list_' . $contact_list->id] = [
        '#type' => 'fieldset',
        '#title' => $this->t('<em>@name</em> list subscription settings', ['@name' => $contact_list->name]),
      ];

      $form['group_contact_list_' . $contact_list->id]['contact_list_' . $contact_list->id] = [
        '#type' => 'select',
        '#options' => ListSubscriptionSetting::getEnabledOptions(),
        '#default_value' => $list_subscription_setting->enabled,
      ];

      $form['group_contact_list_' . $contact_list->id]['contact_list_' . $contact_list->id . '_checked'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('when'),
        '#options' => $checked_options,
        '#default_value' => $list_subscription_setting->requiredCheckedFields,
        '#states' => [
          'visible' => [
            ':input[name="contact_list_' . $contact_list->id . '"]' => ['!value' => ''],
          ],
        ],
      ];
    }

    $tag_options = [];

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Tags $tags_endpoint
     */
    $tags_endpoint = $this->endpointFactory->get(Tags::class);

    try {
      foreach ($tags_endpoint->list() as $tag) {
        $tag_options[$tag->id] = $tag->tag;
      }
    }
    catch (\Exception $e) {
      watchdog_exception('contact_activecampaign', $e);
      $this->messenger()->addError($e->getMessage());
    }

    $form['tags'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact tags'),
      'tag' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Assign the following tags when the form is submitted.'),
        '#options' => $tag_options,
        '#default_value' => $this->contactFormManager->getContactTagIds($this->contactForm, $this->activeCampaignApiAccount),
      ],
    ];

    if (!$this->contactFormManager->supportsAccounts($this->activeCampaignApiAccount)) {
      $this->messenger()
        ->addWarning($this->t('Accounts are not supported by the current ActiveCampaign subscription.'));
    }

    return $form;
  }

  /**
   * Get the send_when options.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   *
   * @return string[]
   *   Tthe options.
   */
  protected function getSendWhenOptions(ContactFormInterface $contact_form): array {
    $send_when_options = [
      'always' => $this->t('Always'),
    ];

    /**
     * @var object[] $fields
     */
    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $contact_form->id());
    $form_display = $this->entityDisplayRepository->getFormDisplay('contact_message', $contact_form->id());

    foreach ($form_display->getComponents() as $field_name => $component) {
      if (!isset($fields[$field_name])) {
        continue;
      }
      if ($fields[$field_name]->getType() == 'boolean') {
        $send_when_options['checkbox_checked:' . $field_name] = $this->t('<em>@field</em> is checked.', ['@field' => strip_tags($fields[$field_name]->getLabel())]);
      }
    }

    return $send_when_options;
  }

  /**
   * Get the field mapping from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The field mapping.
   */
  protected function getFieldMapping(FormStateInterface $form_state): array {
    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $this->contactForm->id());
    $field_mapping = [];

    foreach ($fields as $field_name => $field) {
      $columns = array_keys($field->getFieldStorageDefinition()
        ->getSchema()['columns']);

      foreach ($columns as $column) {
        $value = $form_state->getValue($field_name . ':' . $column);

        if (empty($value)) {
          continue;
        }

        $field_mapping[$field_name . ':' . $column] = $value;
      }
    }

    return $field_mapping;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $determine_ac_contact_method = $form_state->getValue('determine_ac_contact_method');
    $field_mapping = $this->getFieldMapping($form_state);

    if (
      $determine_ac_contact_method === ContactFormManagerInterface::DETERMINE_AC_CONTACT_METHOD_EMAIL &&
      array_search(ContactFormManagerInterface::AC_CONTACT_EMAIL_FIELD_OPTION, $field_mapping) === FALSE
    ) {
      $form_state->setErrorByName('determine_ac_contact_method', $this->t('Please map the ActiveCampaign contact e-mail address property to a field.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->contactForm = $form['#contact_form'];

    $this->contactFormManager->setSendWhen($this->contactForm, $this->activeCampaignApiAccount, $form_state->getValue('send_when'));
    $this->contactFormManager->setFieldMapping($this->contactForm, $this->activeCampaignApiAccount, $this->getFieldMapping($form_state));
    $this->contactFormManager->setEnabled($this->contactForm, $this->activeCampaignApiAccount, $form_state->getValue('enabled'));
    $this->contactFormManager->setDetermineActiveCampaignContactMethod($this->contactForm, $this->activeCampaignApiAccount, $form_state->getValue('determine_ac_contact_method'));
    $this->contactFormManager->setCreateContactWhenNonExisting($this->contactForm, $this->activeCampaignApiAccount, $form_state->getValue('create_nonexisting_contact'));
    if ($this->contactFormManager->supportsAccounts($this->activeCampaignApiAccount)) {
      $this->contactFormManager->setDetermineActiveCampaignAccountMethod($this->contactForm, $this->activeCampaignApiAccount, $form_state->getValue('determine_ac_account_method'));
      $this->contactFormManager->setCreateAccountWhenNonExisting($this->contactForm, $this->activeCampaignApiAccount, $form_state->getValue('create_nonexisting_account'));
    }

    $tag_ids = array_filter($form_state->getValue('tag'), function ($item) {
      return $item != 0;
    });

    $this->contactFormManager->setContactTagIds($this->contactForm, $this->activeCampaignApiAccount, $tag_ids);

    /**
     * @var \Drupal\activecampaign_api\Endpoint\ContactLists $contact_lists_endpoint
     */
    $contact_lists_endpoint = $this->endpointFactory->get(ContactLists::class);

    foreach ($contact_lists_endpoint->list([]) as $contact_list) {
      $list_subscription_setting = new ListSubscriptionSetting();
      $list_subscription_setting->enabled = $form_state->getValue('contact_list_' . $contact_list->id);
      $list_subscription_setting->requiredCheckedFields = $form_state->getValue('contact_list_' . $contact_list->id . '_checked');

      $this->contactFormManager->setListSubscriptionSetting($this->contactForm, $this->activeCampaignApiAccount, $contact_list, $list_subscription_setting);
    }

    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

}
