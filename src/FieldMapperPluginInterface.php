<?php

namespace Drupal\contact_activecampaign;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Interface FieldMapperPluginInterface.
 *
 * Defines the common interface for all field mapper plugin classes.
 *
 * @package Drupal\contact_activecampaign
 */
interface FieldMapperPluginInterface {

  /**
   * Get a list of field types this plugin can map.
   *
   * @return string[]
   *   The field types.
   */
  public function getMappableTypes(): array;

  /**
   * Get a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $fieldItemList
   *   The field.
   * @param int $delta
   *   The delta.
   * @param string $column
   *   The column.
   *
   * @return string
   *   The value.
   */
  public function getFieldValue(FieldItemListInterface $fieldItemList, int $delta, string $column): string;

}
