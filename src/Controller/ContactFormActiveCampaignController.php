<?php

namespace Drupal\contact_activecampaign\Controller;

use Drupal\contact\ContactFormInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Default controller to show an error message when no api accounts added.
 */
class ContactFormActiveCampaignController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Show a message no AC API accounts are configured.
   *
   * @return array
   *   The render array.
   */
  public function page(ContactFormInterface $contact_form) {
    $activecampaign_api_account_storage = $this->entityTypeManager->getStorage('activecampaign_api_account');

    $query = $activecampaign_api_account_storage->getQuery();
    $query->accessCheck(TRUE);
    $query->range(0, 1);
    $activecampaign_api_accounts = $activecampaign_api_account_storage->loadMultiple($query->execute());
    $activecampaign_api_account = reset($activecampaign_api_accounts);
    if (empty($activecampaign_api_account)) {
      $configure_api_accounts_url = Url::fromRoute('entity.activecampaign_api_account.collection');
      return [
        '#markup' => $this->t('No ActiveCampaign API accounts configured. You can do this <a href="@configure_api_accounts_url">here</a>.', ['@configure_api_accounts_url' => $configure_api_accounts_url->toString()]),
      ];
    }

    $url = Url::fromRoute('entity.contact_form.contact_activecampaign_api_account', [
      'contact_form' => $contact_form->id(),
      'activecampaign_api_account' => $activecampaign_api_account->id(),
    ]);

    $response = new RedirectResponse($url->toString());

    return $response;
  }

}
