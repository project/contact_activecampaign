<?php

namespace Drupal\contact_activecampaign;

/**
 * Class ListSubscriptionSetting.
 *
 * Contains AC list subscription settings.
 *
 * @package Drupal\contact_activecampaign
 */
class ListSubscriptionSetting {

  /**
   * The setting.
   *
   * @var string
   */
  public $enabled;

  const IGNORE = '';

  const SUBSCRIBE = 'subscribe';

  const UNSUBSCRIBE = 'unsubscribe';

  /**
   * List of boolean fields which should be checked.
   *
   * @var array
   */
  public $requiredCheckedFields = [];

  /**
   * Get the subscription options for contact lists.
   *
   * @return string[]
   *   The options.
   */
  public static function getEnabledOptions(): array {
    return [
      self::IGNORE => t('Ignore'),
      self::SUBSCRIBE => t('Subscribe'),
      self::UNSUBSCRIBE => t('Unsubscribe'),
    ];
  }

}
