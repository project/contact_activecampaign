<?php

namespace Drupal\contact_activecampaign\Service;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\ContactList;
use Drupal\contact\ContactFormInterface;
use Drupal\contact\Entity\Message;
use Drupal\contact_activecampaign\ListSubscriptionSetting;

/**
 * Interface ContactFormManagerInterface.
 *
 * The interface for the contact form manager.
 *
 * @package Drupal\contact_activecampaign\Service
 */
interface ContactFormManagerInterface {

  const AC_CONTACT_EMAIL_FIELD_OPTION = 'contact:email';

  const DETERMINE_AC_CONTACT_METHOD_EMAIL = 'email';

  const DETERMINE_AC_ACCOUNT_METHOD_NAME = 'name';

  /**
   * Get a list of mappable field types.
   *
   * @return string[]
   *   The list of field types.
   */
  public function getMappableFieldTypes(): array;

  /**
   * Get a list of fields from AC which can be mapped.
   *
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return array
   *   The list of fields.
   */
  public function getMappableFieldOptions(ActivecampaignApiAccountInterface $activecampaignApiAccount): array;

  /**
   * Get the field mapping for the given contact form.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return array
   *   The field mapping.
   */
  public function getFieldMapping(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): array;

  /**
   * Set the field mapping for the given contact form.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param array $fieldMapping
   *   The field mapping.
   */
  public function setFieldMapping(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, array $fieldMapping): void;

  /**
   * Determine if the AC connection is enabled for the given contact form.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return bool
   *   TRUE if enabled, FALSE if not.
   */
  public function isEnabled(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): bool;

  /**
   * Sets if the AC connection is enabled for the given contact form.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param bool $enabled
   *   TRUE if enabled, FALSE if not.
   */
  public function setEnabled(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, bool $enabled): void;

  /**
   * Get determination of when data should be sent to AC.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return string
   *   When data should be sent.
   */
  public function getSendWhen(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): string;

  /**
   * Set determination of when data should be sent to AC.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param string $send_when
   *   When data should be sent.
   */
  public function setSendWhen(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, string $send_when): void;

  /**
   * Get the method to use for determine the current contact in AC.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return string
   *   The method.
   */
  public function getDetermineActiveCampaignContactMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): string;

  /**
   * Set the method to use for determine the current contact in AC.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param string $method
   *   The method.
   */
  public function setDetermineActiveCampaignContactMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, string $method): void;

  /**
   * Determine if an AC contact should be created when none is found already.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return bool
   *   TRUE if contact should be created, FALSE if not.
   */
  public function getCreateContactWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): bool;

  /**
   * Sets if an AC contact should be created when none is found already.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param bool $create
   *   TRUE if contact should be created, FALSE if not.
   */
  public function setCreateContactWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, bool $create): void;

  /**
   * Get the method to use for determine the current account in AC.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return string
   *   The method.
   */
  public function getDetermineActiveCampaignAccountMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): string;

  /**
   * Set the method to use for determine the current account in AC.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param string $method
   *   The method.
   */
  public function setDetermineActiveCampaignAccountMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, string $method): void;

  /**
   * Determine if an AC account should be created when none is found already.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return bool
   *   TRUE if account should be created, FALSE if not.
   */
  public function getCreateAccountWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): bool;

  /**
   * Sets if an AC account should be created when none is found already.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param bool $create
   *   TRUE if account should be created, FALSE if not.
   */
  public function setCreateAccountWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, bool $create): void;

  /**
   * Send the given message to AC.
   *
   * @param \Drupal\contact\Entity\Message $message
   *   The message.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Contact|null
   *   The created/updated contact, or null if it is not enabled.
   */
  public function sendToActiveCampaign(Message $message, ActivecampaignApiAccountInterface $activecampaignApiAccount): ?Contact;

  /**
   * Get the list subscription setting for the given contact_form/list.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param \Drupal\activecampaign_api\ApiResource\ContactList $list
   *   The list.
   *
   * @return \Drupal\contact_activecampaign\ListSubscriptionSetting
   *   The setting.
   */
  public function getListSubscriptionSetting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, ContactList $list): ListSubscriptionSetting;

  /**
   * Set the list subscription setting for the given contact_form/list.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param \Drupal\activecampaign_api\ApiResource\ContactList $list
   *   The list.
   * @param \Drupal\contact_activecampaign\ListSubscriptionSetting $listSubscriptionSetting
   *   The setting.
   */
  public function setListSubscriptionSetting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, ContactList $list, ListSubscriptionSetting $listSubscriptionSetting): void;

  /**
   * Get the tags the contact should have added after submitting the form.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return string[]
   *   The list of tag ids.
   */
  public function getContactTagIds(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): array;

  /**
   * Set the tags the contact should have added after submitting the form.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param array $tag_ids
   *   The list of tag ids.
   */
  public function setContactTagIds(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, array $tag_ids): void;

  /**
   * Check if the AC subscription supports accounts.
   *
   * The meta.total field is missing from the response if not. According to
   * AC, there is no better way to figure this out.
   *
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return bool
   *   True or false.
   */
  public function supportsAccounts(ActivecampaignApiAccountInterface $activecampaignApiAccount): bool;

}
