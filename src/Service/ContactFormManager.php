<?php

namespace Drupal\contact_activecampaign\Service;

use Drupal\activecampaign_api\ActivecampaignApiAccountInterface;
use Drupal\activecampaign_api\ApiResource\Account;
use Drupal\activecampaign_api\ApiResource\AccountContact;
use Drupal\activecampaign_api\ApiResource\AccountCustomFieldValue;
use Drupal\activecampaign_api\ApiResource\Contact;
use Drupal\activecampaign_api\ApiResource\ContactList;
use Drupal\activecampaign_api\ApiResource\ContactTag;
use Drupal\activecampaign_api\ApiResource\FieldValue;
use Drupal\activecampaign_api\Endpoint\AccountContacts;
use Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta;
use Drupal\activecampaign_api\Endpoint\Accounts;
use Drupal\activecampaign_api\Endpoint\ContactLists;
use Drupal\activecampaign_api\Endpoint\Contacts;
use Drupal\activecampaign_api\Endpoint\ContactTags;
use Drupal\activecampaign_api\Endpoint\Fields;
use Drupal\activecampaign_api\Service\EndpointFactoryInterface;
use Drupal\contact\ContactFormInterface;
use Drupal\contact\Entity\Message;
use Drupal\contact_activecampaign\FieldMapperPluginManager;
use Drupal\contact_activecampaign\ListSubscriptionSetting;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Interface ContactFormManagerInterface.
 *
 * The contact form manager service.
 *
 * @package Drupal\contact_activecampaign\Service
 */
class ContactFormManager implements ContactFormManagerInterface {

  use StringTranslationTrait;

  /**
   * The AC endpoint factory.
   *
   * @var \Drupal\activecampaign_api\Service\EndpointFactoryInterface
   */
  protected $endpointFactory;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Field mapper plugin manager.
   *
   * @var \Drupal\contact_activecampaign\FieldMapperPluginManager
   */
  protected $fieldMapperPluginManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ContactFormManagerInterface constructor.
   *
   * @param \Drupal\activecampaign_api\Service\EndpointFactoryInterface $endpointFactory
   *   The AC endpoint factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\contact_activecampaign\FieldMapperPluginManager $fieldMapperPluginManager
   *   Field mapper plugin manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(EndpointFactoryInterface $endpointFactory, LoggerChannelFactoryInterface $loggerChannelFactory, MessengerInterface $messenger, FieldMapperPluginManager $fieldMapperPluginManager, RequestStack $requestStack, ModuleHandlerInterface $moduleHandler, ConfigFactoryInterface $configFactory) {
    $this->endpointFactory = clone $endpointFactory;
    $this->logger = $loggerChannelFactory->get('contact_activecampaign');
    $this->messenger = $messenger;
    $this->fieldMapperPluginManager = $fieldMapperPluginManager;
    $this->requestStack = $requestStack;
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappableFieldTypes(): array {
    $mappable_field_types = [];
    foreach ($this->fieldMapperPluginManager->getDefinitions() as $definition) {
      /**
       * @var \Drupal\contact_activecampaign\FieldMapperPluginInterface $plugin
       */
      $plugin = $this->fieldMapperPluginManager->createInstance($definition['id']);
      foreach ($plugin->getMappableTypes() as $mappableType) {
        if (!in_array($mappableType, $mappable_field_types)) {
          $mappable_field_types[] = $mappableType;
        }
      }
    }

    return $mappable_field_types;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappableFieldOptions(ActivecampaignApiAccountInterface $activecampaignApiAccount): array {
    $this->endpointFactory->setActivecampaignApiAccount($activecampaignApiAccount);

    // Get contact fields.
    /**
     * @var \Drupal\activecampaign_api\Endpoint\Fields $fields_endpoint
     */
    $fields_endpoint = $this->endpointFactory->get(Fields::class);

    $contact_field_options = [
      self::AC_CONTACT_EMAIL_FIELD_OPTION => $this->t('E-mail address'),
      'contact:firstName' => $this->t('First name'),
      'contact:lastName' => $this->t('Last name'),
      'contact:phone' => $this->t('Telephone'),
    ];
    try {
      foreach ($fields_endpoint->list([]) as $field) {
        $contact_field_options['field:' . $field->id] = $field->title;
      }
    }
    catch (\Exception $e) {
      watchdog_exception('contact_activecampaign', $e);
      $this->messenger->addError($e->getMessage());
    }
    asort($contact_field_options);

    // Get account fields.
    try {
      /**
       * @var \Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta $accountcustomfieldmeta_endpoint
       */
      $accountcustomfieldmeta_endpoint = $this->endpointFactory->get(AccountCustomFieldMeta::class);
      $account_field_options = [
        'account:name' => $this->t('Name'),
      ];

      foreach ($accountcustomfieldmeta_endpoint->list([]) as $accountcustomfieldmeta) {
        $account_field_options['accountcustomfieldmeta:' . $accountcustomfieldmeta->id] = $accountcustomfieldmeta->fieldLabel;
      }
      asort($account_field_options);

    }
    catch (\Exception $e) {
      if (!$e->getMessage() === 'meta.total is missing') {
        // meta.total is missing means accounts are not supported.
        watchdog_exception('contact_activecampaign', $e);
        $this->messenger->addError($e->getMessage());
      }
      $account_field_options = [];
    }

    $fields = [
      '' => $this->t('None'),
      'Contact' => $contact_field_options,
    ];

    if (!empty($account_field_options)) {
      $fields['Account'] = $account_field_options;
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsAccounts(ActivecampaignApiAccountInterface $activecampaignApiAccount): bool {
    static $cache = NULL;
    $this->endpointFactory->setActivecampaignApiAccount($activecampaignApiAccount);

    if ($cache === NULL) {
      /**
       * @var \Drupal\activecampaign_api\Endpoint\AccountCustomFieldMeta $accountcustomfieldmeta_endpoint
       */
      $accountcustomfieldmeta_endpoint = $this->endpointFactory->get(AccountCustomFieldMeta::class);
      try {
        $accountcustomfieldmeta_endpoint->list([]);
        $cache = TRUE;
      }
      catch (\Exception $e) {
        $cache = FALSE;
      }
    }
    return $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapping(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): array {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    $field_mapping = $settings->get('field_mapping');
    if (!is_array($field_mapping)) {
      return [];
    }
    return $field_mapping;
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMapping(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, array $fieldMapping): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('field_mapping', $fieldMapping);
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): bool {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    return $settings->get('enabled') == TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, bool $enabled): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('enabled', $enabled);
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getSendWhen(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): string {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    $send_when = $settings->get('send_when');
    if (empty($send_when)) {
      $send_when = 'always';
    }
    return $send_when;
  }

  /**
   * {@inheritdoc}
   */
  public function setSendWhen(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, string $send_when): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('send_when', $send_when);
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getDetermineActiveCampaignContactMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): string {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    return $settings->get('determine_ac_contact_method') ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setDetermineActiveCampaignContactMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, string $method): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('determine_ac_contact_method', $method);
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateContactWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): bool {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    return $settings->get('create_nonexisting') == TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreateContactWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, bool $create): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('create_nonexisting', $create);
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getDetermineActiveCampaignAccountMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): string {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    return $settings->get('determine_ac_account_method') ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setDetermineActiveCampaignAccountMethod(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, string $method): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('determine_ac_account_method', $method);
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateAccountWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): bool {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    return $settings->get('create_nonexisting_account') == TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreateAccountWhenNonExisting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, bool $create): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('create_nonexisting_account', $create);
    $settings->save();
  }

  /**
   * Get the AC contact for the e-mail address entered in the form.
   *
   * @param \Drupal\contact\Entity\Message $message
   *   The form submission.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Contact|null
   *   The contact or null if it doesn't exist.
   */
  protected function getActiveCampaignContactByEmail(Message $message, ActivecampaignApiAccountInterface $activecampaignApiAccount): ?Contact {
    $this->endpointFactory->setActivecampaignApiAccount($activecampaignApiAccount);
    /**
     * @var \Drupal\activecampaign_api\Endpoint\Contacts $contacts_endpoint
     */
    $contacts_endpoint = $this->endpointFactory->get(Contacts::class);

    $field_mapping = $this->getFieldMapping($message->getContactForm(), $activecampaignApiAccount);

    $contact_form_field_mapped_to_email = array_search(self::AC_CONTACT_EMAIL_FIELD_OPTION, $field_mapping);

    if ($contact_form_field_mapped_to_email === FALSE) {
      return NULL;
    }

    $email_field_name_column = explode(':', $contact_form_field_mapped_to_email);
    $email_field = $email_field_name_column[0];
    $email_column = $email_field_name_column[1];

    if (empty($email_field) || !$message->hasField($email_field)) {
      return NULL;
    }

    $email_value = $message->get($email_field)->getValue();
    $email_value = $email_value[$email_column] ?? $message->get($email_field)
      ->getString();

    $contacts = $contacts_endpoint->list([
      'email' => $email_value,
    ]);

    /**
     * @var \Drupal\activecampaign_api\ApiResource\Contact|boolean
     */
    $contact = reset($contacts);

    return $contact !== FALSE ? $contact : NULL;
  }

  /**
   * Get the AC account with the given name.
   *
   * @param string $name
   *   The name.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account interface.
   *
   * @return \Drupal\activecampaign_api\ApiResource\Account|null
   *   The account or null if not found.
   */
  protected function getActiveCampaignAccountByName(string $name, ActivecampaignApiAccountInterface $activecampaignApiAccount): ?Account {
    $this->endpointFactory->setActivecampaignApiAccount($activecampaignApiAccount);
    if (!$this->supportsAccounts($activecampaignApiAccount)) {
      return NULL;
    }
    /**
     * @var \Drupal\activecampaign_api\Endpoint\Accounts $accounts_endpoint
     */
    $accounts_endpoint = $this->endpointFactory->get(Accounts::class);

    /**
     * @var \Drupal\activecampaign_api\ApiResource\Account|false
     */
    $activecampaign_account = current($accounts_endpoint->list(['name' => $name]));

    if ($activecampaign_account === FALSE) {
      return NULL;
    }
    return $activecampaign_account;
  }

  /**
   * Context information for sendToActiveCampaign.
   *
   * @var array|null
   */
  protected static $sendToActiveCampaignContext = NULL;

  /**
   * Get the context information for sendToActiveCampaign.
   *
   * @return array|null
   *   The context information.
   */
  public static function getSendToActiveCampaignContext(): ?array {
    return self::$sendToActiveCampaignContext;
  }

  /**
   * Determine if the given message should be sent to AC.
   *
   * @param \Drupal\contact\Entity\Message $message
   *   The message.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   *
   * @return bool
   *   Should send=true.
   */
  public function shouldSendToActiveCampaign(Message $message, ActivecampaignApiAccountInterface $activecampaignApiAccount): bool {
    $contact_form = $message->getContactForm();

    if (!$this->isEnabled($contact_form, $activecampaignApiAccount)) {
      return FALSE;
    }

    $send_when = $this->getSendWhen($contact_form, $activecampaignApiAccount);
    $send_when_splitted = explode(':', $send_when);

    switch ($send_when_splitted[0]) {
      case 'always':
        return TRUE;

      case 'checkbox_checked':
        if (!$message->hasField($send_when_splitted[1]) || $message->get($send_when_splitted[1])
          ->getString() != '1') {
          return FALSE;
        }
        return TRUE;

      default:
        throw new \InvalidArgumentException('Invalid send_when: ' . $send_when);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendToActiveCampaign(Message $message, ActivecampaignApiAccountInterface $activecampaignApiAccount): ?Contact {
    $contact_form = $message->getContactForm();
    $this->endpointFactory->setActivecampaignApiAccount($activecampaignApiAccount);

    if (!$this->shouldSendToActiveCampaign($message, $activecampaignApiAccount)) {
      return NULL;
    }

    self::$sendToActiveCampaignContext = [
      'contact_form' => $contact_form,
      'contact_message' => $message,
    ];

    $determine_ac_contact_method = $this->getDetermineActiveCampaignContactMethod($contact_form, $activecampaignApiAccount);
    $determine_ac_account_method = $this->getDetermineActiveCampaignAccountMethod($contact_form, $activecampaignApiAccount);
    $create_nonexisting_contact = $this->getCreateContactWhenNonExisting($contact_form, $activecampaignApiAccount);
    $create_nonexisting_account = $this->getCreateAccountWhenNonExisting($contact_form, $activecampaignApiAccount);
    $field_mapping = $this->getFieldMapping($contact_form, $activecampaignApiAccount);

    /**
     * @var \Drupal\activecampaign_api\Endpoint\Contacts $contacts_endpoint
     */
    $contacts_endpoint = $this->endpointFactory->get(Contacts::class);

    /**
     * @var \Drupal\activecampaign_api\Endpoint\AccountContacts $accountcontacts_endpoint
     */
    $accountcontacts_endpoint = $this->endpointFactory->get(AccountContacts::class);
    /**
     * @var \Drupal\activecampaign_api\Endpoint\Accounts $accounts_endpoint
     */
    $accounts_endpoint = $this->endpointFactory->get(Accounts::class);

    switch ($determine_ac_contact_method) {
      case self::DETERMINE_AC_CONTACT_METHOD_EMAIL:
        $current_ac_contact = $this->getActiveCampaignContactByEmail($message, $activecampaignApiAccount);
        break;

      default:
        throw new \InvalidArgumentException('Unknown determine_ac_contact_method ' . $determine_ac_contact_method);
    }

    if ($current_ac_contact === NULL) {
      // No current contact could be determined.
      if (!$create_nonexisting_contact) {
        // We're not allowed to create contacts in AC.
        return NULL;
      }

      // Create an instance for the new contacts data. The ID is empty, so we
      // can use it to determine if the contact should be created or updated
      // later.
      $current_ac_contact = new Contact();
    }

    if (!empty($current_ac_contact->id) && $this->supportsAccounts($activecampaignApiAccount)) {
      /**
       * @var \Drupal\activecampaign_api\ApiResource\AccountContact|false $current_ac_accountcontact
       */
      $current_ac_accountcontact = current($accountcontacts_endpoint->getAllByContact($current_ac_contact));
      if ($current_ac_accountcontact === FALSE) {
        $current_ac_account = new Account();
      }
      else {
        $current_ac_account = $accounts_endpoint->get($current_ac_accountcontact->account);
        if ($current_ac_account === NULL) {
          throw new \RuntimeException('Account ' . $current_ac_accountcontact->account . ' linked to contact ' . $current_ac_accountcontact->contact . ', but doesnt seem to exist.');
        }
      }
    }
    else {
      $current_ac_account = new Account();
    }

    $required_contact_linked_to_account = FALSE;

    $field_types_field_mapper = [];
    foreach ($this->fieldMapperPluginManager->getDefinitions() as $definition) {
      /**
       * @var \Drupal\contact_activecampaign\FieldMapperPluginInterface $plugin
       */
      $plugin = $this->fieldMapperPluginManager->createInstance($definition['id']);
      foreach ($plugin->getMappableTypes() as $field_type) {
        $field_types_field_mapper[$field_type] = $plugin;
      }
    }

    foreach ($field_mapping as $field_column => $ac_property) {
      $field_name_column = explode(':', $field_column);
      $field = $field_name_column[0];
      $column = $field_name_column[1];

      if (empty($field) || !$message->hasField($field)) {
        continue;
      }

      [$group, $property] = explode(':', $ac_property);
      $field_instance = $message->get($field);

      if (!isset($field_types_field_mapper[$field_instance->getFieldDefinition()
        ->getType()])) {
        continue;
      }

      $value = $field_types_field_mapper[$field_instance->getFieldDefinition()
        ->getType()]->getFieldValue($field_instance, 0, $column);

      if (empty($value)) {
        // Skip empty values.
        continue;
      }

      $activecampaign_property = [
        'group' => $group,
        'property' => $property,
      ];

      $this->moduleHandler->alter(
        'contact_activecampaign_mapped_field_value',
        $value,
        $field_instance,
        $activecampaign_property
      );

      switch ($group) {
        case 'contact':
          $current_ac_contact->$property = $value;
          break;

        case 'field':
          $current_ac_contact->fieldValues[] = new FieldValue(
            $property,
            $value
          );
          break;

        case 'account':
          if ($this->supportsAccounts($activecampaignApiAccount)) {
            $required_contact_linked_to_account = TRUE;
            $current_ac_account->$property = $value;
          }
          break;

        case 'accountcustomfieldmeta':
          if ($this->supportsAccounts($activecampaignApiAccount)) {
            $required_contact_linked_to_account = TRUE;
            $current_ac_account->fields[] = new AccountCustomFieldValue(
              $property,
              $value
            );
          }
          break;

        default:
          throw new \InvalidArgumentException('Unknown group ' . $group);
      }
    }

    if (empty($current_ac_contact->id)) {
      try {
        // Create the contact.
        $current_ac_contact = $contacts_endpoint->create($current_ac_contact);

        $this->logger->info('Created a new ActiveCampaign contact in @activecampaign_api_account @contact_id for contact_message @message_id', [
          '@activecampaign_api_account' => $activecampaignApiAccount->id(),
          '@contact_id' => $current_ac_contact->id,
          '@message_id' => $message->id(),
        ]);
      }
      catch (\Exception $e) {
        $this->logger->info('Failed creating a new ActiveCampaign contact in @activecampaign_api_account for contact_message @message_id', [
          '@activecampaign_api_account' => $activecampaignApiAccount->id(),
          '@message_id' => $message->id(),
        ]);
        throw $e;
      }
    }
    else {
      try {
        // Update the contact.
        $contacts_endpoint->update($current_ac_contact);

        $this->logger->info('Updated ActiveCampaign contact @contact_id in @activecampaign_api_account for contact_message @message_id', [
          '@activecampaign_api_account' => $activecampaignApiAccount->id(),
          '@contact_id' => $current_ac_contact->id,
          '@message_id' => $message->id(),
        ]);
      }
      catch (\Exception $e) {
        $this->logger->info('Failing updating ActiveCampaign contact in @activecampaign_api_account @contact_id for contact_message @message_id', [
          '@activecampaign_api_account' => $activecampaignApiAccount->id(),
          '@contact_id' => $current_ac_contact->id,
          '@message_id' => $message->id(),
        ]);
        throw $e;
      }
    }

    if ($required_contact_linked_to_account && $this->supportsAccounts($activecampaignApiAccount)) {
      // We want to store data into the account, so update it here.
      if (empty($current_ac_account->id)) {
        // Check if there already is an account for the entered account name.
        if (!empty($current_ac_account->name)) {
          switch ($determine_ac_account_method) {
            case self::DETERMINE_AC_ACCOUNT_METHOD_NAME:
              /**
               * @var \Drupal\activecampaign_api\ApiResource\Account|null
               */
              $activecampaign_account = $this->getActiveCampaignAccountByName($current_ac_account->name, $activecampaignApiAccount);
              break;

            default:
              throw new \InvalidArgumentException('Unknown determine_ac_account_method ' . $determine_ac_account_method);
          }

          if ($activecampaign_account !== NULL) {
            $current_ac_account->id = $activecampaign_account->id;
          }
        }

        if (empty($current_ac_account->id) && $create_nonexisting_account) {
          try {
            // Create the account.
            if (empty($current_ac_account->name)) {
              // Account name cannot be empty. Generate a dummy name.
              $current_ac_account->name = $this->t('Company for contact_message @message_id at @host', [
                '@message_id' => $message->id(),
                '@host' => $this->requestStack->getCurrentRequest()->getHost(),
              ]);
            }

            $current_ac_account = $accounts_endpoint->create($current_ac_account);

            $this->logger->info('Created a new ActiveCampaign account @account_id in @activecampaign_api_account for contact_message @message_id', [
              '@activecampaign_api_account' => $activecampaignApiAccount->id(),
              '@account_id' => $current_ac_account->id,
              '@message_id' => $message->id(),
            ]);
          }
          catch (\Exception $e) {
            $this->logger->info('Failed creating a new ActiveCampaign account in @activecampaign_api_account for contact_message @message_id', [
              '@activecampaign_api_account' => $activecampaignApiAccount->id(),
              '@message_id' => $message->id(),
            ]);
            throw $e;
          }
        }

        if (!empty($current_ac_account->id)) {
          try {
            // Link the account to the contact.
            $current_ac_accountcontact = new AccountContact();
            $current_ac_accountcontact->account = $current_ac_account->id;
            $current_ac_accountcontact->contact = $current_ac_contact->id;
            $current_ac_accountcontact->jobTitle = '';
            $current_ac_accountcontact = $accountcontacts_endpoint->create($current_ac_accountcontact);

            $this->logger->info('Linked a ActiveCampaign account @account_id in @activecampaign_api_account to contact @contact_id for contact_message @message_id', [
              '@activecampaign_api_account' => $activecampaignApiAccount->id(),
              '@account_id' => $current_ac_account->id,
              '@contact_id' => $current_ac_contact->id,
              '@message_id' => $message->id(),
            ]);
          }
          catch (\Exception $e) {
            $this->logger->info('Failed linking a ActiveCampaign account @account_id in @activecampaign_api_account to contact @contact_id for contact_message @message_id', [
              '@activecampaign_api_account' => $activecampaignApiAccount->id(),
              '@account_id' => $current_ac_account->id,
              '@contact_id' => $current_ac_contact->id,
              '@message_id' => $message->id(),
            ]);
            throw $e;
          }
        }
      }
      else {
        try {
          // Update the account.
          $accounts_endpoint->update($current_ac_account);

          $this->logger->info('Updated ActiveCampaign account @account_id in @activecampaign_api_account for contact_message @message_id', [
            '@activecampaign_api_account' => $activecampaignApiAccount->id(),
            '@account_id' => $current_ac_account->id,
            '@message_id' => $message->id(),
          ]);
        }
        catch (\Exception $e) {
          $this->logger->info('Failing updating ActiveCampaign contact @account_id in @activecampaign_api_account for contact_message @message_id', [
            '@activecampaign_api_account' => $activecampaignApiAccount->id(),
            '@account_id' => $current_ac_account->id,
            '@message_id' => $message->id(),
          ]);
          throw $e;
        }
      }
    }

    // Update list subscriptions.
    /**
     * @var \Drupal\activecampaign_api\Endpoint\ContactLists $contact_list_endpoint
     */
    $contact_list_endpoint = $this->endpointFactory->get(ContactLists::class);

    foreach ($contact_list_endpoint->list([]) as $contact_list) {
      $list_subscription_setting = $this->getListSubscriptionSetting($contact_form, $activecampaignApiAccount, $contact_list);
      if ($list_subscription_setting->enabled === ListSubscriptionSetting::IGNORE) {
        continue;
      }

      $has_checked_all_required_fields = TRUE;
      foreach ($list_subscription_setting->requiredCheckedFields as $field => $checked) {
        if (empty($checked)) {
          continue;
        }
        if (!$message->hasField($field)) {
          // Field does not exist (anymore).
          $this->logger->warning('Field ' . $field . ' does not exist on entity type ' . $message->getEntityTypeId() . '/' . $message->bundle());
          continue;
        }

        if ($message->get($field)->getString() !== '1') {
          // Not checked.
          $has_checked_all_required_fields = FALSE;
          break;
        }
      }

      if (!$has_checked_all_required_fields) {
        continue;
      }

      if ($list_subscription_setting->enabled === ListSubscriptionSetting::SUBSCRIBE) {
        $contact_list_endpoint->updateStatus(
          $contact_list,
          $current_ac_contact,
          TRUE
        );
      }
      elseif ($list_subscription_setting->enabled === ListSubscriptionSetting::UNSUBSCRIBE) {
        $contact_list_endpoint->updateStatus(
          $contact_list,
          $current_ac_contact,
          FALSE
        );
      }
      else {
        throw new \InvalidArgumentException('Unknown value ' . $list_subscription_setting->enabled);
      }
    }

    // Add tags, if any.
    $tag_ids = $this->getContactTagIds($contact_form, $activecampaignApiAccount);

    if (count($tag_ids) > 0) {
      /**
       * @var \Drupal\activecampaign_api\Endpoint\ContactTags $contact_tags_endpoint
       */
      $contact_tags_endpoint = $this->endpointFactory->get(ContactTags::class);

      foreach ($tag_ids as $tag_id) {
        $contact_tag = new ContactTag();
        $contact_tag->contact = $current_ac_contact->id;
        $contact_tag->tag = $tag_id;

        try {
          $contact_tags_endpoint->create($contact_tag);
        }
        catch (\Exception $e) {
          // Ignore missing tags.
          if (stripos($e->getMessage(), 'Tag not found') === FALSE) {
            throw $e;
          }
        }
      }
    }

    return $current_ac_contact;
  }

  /**
   * {@inheritdoc}
   */
  public function getListSubscriptionSetting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, ContactList $list): ListSubscriptionSetting {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    try {
      $list_subscription = unserialize($settings->get('list_subscription') ?? '', ['allowed_classes' => [ListSubscriptionSetting::class]]);
      if (!is_array($list_subscription)) {
        $list_subscription = [];
      }
    }
    catch (\Exception $e) {
      $list_subscription = [];
    }

    if (isset($list_subscription[$list->id])) {
      return $list_subscription[$list->id];
    }

    $list_subscription = new ListSubscriptionSetting();
    $list_subscription->enabled = ListSubscriptionSetting::IGNORE;

    return $list_subscription;
  }

  /**
   * {@inheritdoc}
   */
  public function setListSubscriptionSetting(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, ContactList $list, ListSubscriptionSetting $listSubscriptionSetting): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    try {
      $list_subscription = unserialize($settings->get('list_subscription') ?? '', ['allowed_classes' => [ListSubscriptionSetting::class]]);
      if (!is_array($list_subscription)) {
        $list_subscription = [];
      }
    }
    catch (\Exception $e) {
      $list_subscription = [];
    }

    $list_subscription[$list->id] = $listSubscriptionSetting;

    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('list_subscription', serialize($list_subscription));
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getContactTagIds(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount): array {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount);
    $tag_ids = $settings->get('tag_ids');
    if (!is_array($tag_ids)) {
      return [];
    }
    return $tag_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function setContactTagIds(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, array $tag_ids): void {
    $settings = $this->getConfig($contactForm, $activecampaignApiAccount, TRUE);
    $settings->set('tag_ids', $tag_ids);
    $settings->save();
  }

  /**
   * Get the config object.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   * @param \Drupal\activecampaign_api\ActivecampaignApiAccountInterface $activecampaignApiAccount
   *   The activecampaign api account.
   * @param bool $editable
   *   Get editable config object?
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   The config object.
   */
  protected function getConfig(ContactFormInterface $contactForm, ActivecampaignApiAccountInterface $activecampaignApiAccount, bool $editable = FALSE) {
    $key = 'contact_activecampaign.' . $contactForm->id() . '.' . $activecampaignApiAccount->id();
    if ($editable) {
      return $this->configFactory->getEditable($key);
    }
    return $this->configFactory->get($key);
  }

}
