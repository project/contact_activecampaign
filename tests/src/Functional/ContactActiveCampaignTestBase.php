<?php

namespace Drupal\Tests\contact_activecampaign\Functional;

use Drupal\activecampaign_api\Entity\ActivecampaignApiApiAccount;
use Drupal\contact\Entity\ContactForm;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Class ContactActiveCampaignTestBase.
 *
 * Test base class.
 *
 * @package Drupal\Tests\contact_activecampaign\Functional
 */
abstract class ContactActiveCampaignTestBase extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'contact_activecampaign',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Contact form.
   *
   * @var \Drupal\contact\Entity\ContactForm
   */
  protected $contactForm;

  /**
   * Contact form form display.
   *
   * @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   */
  protected $contactFormFormDisplay;

  /**
   * The contact form manager.
   *
   * @var \Drupal\contact_activecampaign\Service\ContactFormManager
   */
  protected $contactFormManager;

  /**
   * Test activecampaign api account.
   *
   * @var \Drupal\activecampaign_api\ActivecampaignApiAccountInterface
   */
  protected $activecampaignApiAccount;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityDisplayRepository = $this->container->get('entity_display.repository');

    $this->contactForm = ContactForm::create([
      'label' => 'Test contact form',
      'id' => 'test_contact_form',
    ]);
    $this->contactForm->save();

    // field_text1.
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'contact_message',
      'field_name' => 'field_text1',
      'type' => 'text_long',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'entity_type' => 'contact_message',
      'bundle' => $this->contactForm->id(),
      'field_name' => 'field_text1',
      'label' => 'field_text1',
      'required' => FALSE,
    ]);
    $field->save();

    // field_text2.
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'contact_message',
      'field_name' => 'field_text2',
      'type' => 'text_long',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'entity_type' => 'contact_message',
      'bundle' => $this->contactForm->id(),
      'field_name' => 'field_text2',
      'label' => 'field_text2',
      'required' => FALSE,
    ]);
    $field->save();

    // field_boolean.
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'contact_message',
      'field_name' => 'field_boolean',
      'type' => 'boolean',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'entity_type' => 'contact_message',
      'bundle' => $this->contactForm->id(),
      'field_name' => 'field_boolean',
      'label' => 'field_boolean',
      'required' => FALSE,
    ]);
    $field->save();

    $this->contactFormFormDisplay = $this->entityDisplayRepository->getFormDisplay('contact_message', $this->contactForm->id());

    $this->contactFormFormDisplay->setComponent('field_text1', [
      'type' => 'text_textfield',
    ]);
    $this->contactFormFormDisplay->setComponent('field_text2', [
      'type' => 'text_textfield',
    ]);

    $this->contactFormFormDisplay->save();
    $this->contactForm->save();

    $this->contactFormManager = $this->container->get('contact_activecampaign.contact_form_manager');

    $this->activecampaignApiAccount = ActivecampaignApiApiAccount::create([
      'id' => 'activecampaign_api_test',
      'label' => 'activecampaign_api_test',
      'base_url' => $_ENV['ACTIVECAMPAIGN_API_TEST_BASE_URL'] ?? 'https://groowup-sandbox.api-us1.com/api/3',
      'api_token' => $_ENV['ACTIVECAMPAIGN_API_TEST_API_TOKEN'] ?? 'c6ae6befbe03e775ab1daf46b68a5ae3bfdb97bd39f4df7c304b9a2258c4223ea229f20a',
      'event_tracking_base_url' => $_ENV['ACTIVECAMPAIGN_API_TEST_EVENT_TRACKING_BASE_URL'] ?? 'https://trackcmp.net/event',
      'event_tracking_key' => $_ENV['ACTIVECAMPAIGN_API_TEST_EVENT_TRACKING_KEY'] ?? '5cfac018b95a9769d1caa56ef9a954e2f268beb0',
      'event_tracking_actid' => $_ENV['ACTIVECAMPAIGN_API_TEST_EVENT_TRACKING_ACTID'] ?? '478462387',
    ]);
    $this->activecampaignApiAccount->save();
  }

}
