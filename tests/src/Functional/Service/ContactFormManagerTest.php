<?php

namespace Drupal\Tests\contact_activecampaign\Functional\Service;

use Drupal\activecampaign_api\ApiResource\ContactList;
use Drupal\contact\Entity\Message;
use Drupal\contact_activecampaign\ListSubscriptionSetting;
use Drupal\Tests\contact_activecampaign\Functional\ContactActiveCampaignTestBase;

/**
 * Class ContactFormManagerTest.
 *
 * Tests the ContactFormManager service.
 *
 * @package Drupal\Tests\contact_activecampaign\Functional\Service
 * @group contact_activecampaign
 */
class ContactFormManagerTest extends ContactActiveCampaignTestBase {

  /**
   * TestFieldMapping.
   */
  public function testFieldMapping(): void {
    $mapping = $this->contactFormManager->getFieldMapping($this->contactForm, $this->activecampaignApiAccount);
    $this->assertIsArray($mapping);
    $this->assertCount(0, $mapping);
    $mapping = [1 => 2, 'a' => 'b'];
    $this->contactFormManager->setFieldMapping($this->contactForm, $this->activecampaignApiAccount, $mapping);
    $this->assertEquals($mapping, $this->contactFormManager->getFieldMapping($this->contactForm, $this->activecampaignApiAccount));
  }

  /**
   * TestEnabled.
   */
  public function testEnabled(): void {
    $this->assertFalse($this->contactFormManager->isEnabled($this->contactForm, $this->activecampaignApiAccount));
    $this->contactFormManager->setEnabled($this->contactForm, $this->activecampaignApiAccount, TRUE);
    $this->assertTrue($this->contactFormManager->isEnabled($this->contactForm, $this->activecampaignApiAccount));
    $this->contactFormManager->setEnabled($this->contactForm, $this->activecampaignApiAccount, FALSE);
    $this->assertFalse($this->contactFormManager->isEnabled($this->contactForm, $this->activecampaignApiAccount));
  }

  /**
   * TestSendWhen.
   */
  public function testSendWhen(): void {
    $this->assertEquals('always', $this->contactFormManager->getSendWhen($this->contactForm, $this->activecampaignApiAccount));
    $this->contactFormManager->setSendWhen($this->contactForm, $this->activecampaignApiAccount, 'abc');
    $this->assertEquals('abc', $this->contactFormManager->getSendWhen($this->contactForm, $this->activecampaignApiAccount));
    $this->contactFormManager->setSendWhen($this->contactForm, $this->activecampaignApiAccount, '');
    $this->assertEquals('always', $this->contactFormManager->getSendWhen($this->contactForm, $this->activecampaignApiAccount));
  }

  /**
   * TestDetermineActiveCampaignContactMethod.
   */
  public function testDetermineActiveCampaignContactMethod(): void {
    $this->assertEquals('', $this->contactFormManager->getDetermineActiveCampaignContactMethod($this->contactForm, $this->activecampaignApiAccount));
    $this->contactFormManager->setDetermineActiveCampaignContactMethod($this->contactForm, $this->activecampaignApiAccount, 'email');
    $this->assertEquals('email', $this->contactFormManager->getDetermineActiveCampaignContactMethod($this->contactForm, $this->activecampaignApiAccount));
  }

  /**
   * TestCreateWhenNonExisting.
   */
  public function testCreateWhenNonExisting(): void {
    $this->assertFalse($this->contactFormManager->getCreateContactWhenNonExisting($this->contactForm, $this->activecampaignApiAccount));
    $this->contactFormManager->setCreateContactWhenNonExisting($this->contactForm, $this->activecampaignApiAccount, TRUE);
    $this->assertTrue($this->contactFormManager->getCreateContactWhenNonExisting($this->contactForm, $this->activecampaignApiAccount));
    $this->contactFormManager->setCreateContactWhenNonExisting($this->contactForm, $this->activecampaignApiAccount, FALSE);
    $this->assertFalse($this->contactFormManager->getCreateContactWhenNonExisting($this->contactForm, $this->activecampaignApiAccount));
  }

  /**
   * TestListSubscriptionSetting.
   */
  public function testListSubscriptionSetting(): void {
    $list = new ContactList();
    $list->id = '123';
    $instance = $this->contactFormManager->getListSubscriptionSetting($this->contactForm, $this->activecampaignApiAccount, $list);
    $this->assertInstanceOf(ListSubscriptionSetting::class, $instance);
    $this->assertEquals(ListSubscriptionSetting::IGNORE, $instance->enabled);
    $this->assertIsArray($instance->requiredCheckedFields);
    $instance->enabled = ListSubscriptionSetting::SUBSCRIBE;
    $instance->requiredCheckedFields = ['a', 'b'];
    $this->contactFormManager->setListSubscriptionSetting($this->contactForm, $this->activecampaignApiAccount, $list, $instance);
    $this->assertEquals($instance, $this->contactFormManager->getListSubscriptionSetting($this->contactForm, $this->activecampaignApiAccount, $list));
  }

  /**
   * TestShouldSendToActiveCampaign.
   */
  public function testShouldSendToActiveCampaign(): void {
    $this->contactFormManager->setEnabled($this->contactForm, $this->activecampaignApiAccount, FALSE);
    $this->assertFalse($this->contactFormManager->shouldSendToActiveCampaign(Message::create([
      'contact_form' => $this->contactForm->id(),
      'field_boolean' => FALSE,
    ]), $this->activecampaignApiAccount));
    $this->assertFalse($this->contactFormManager->shouldSendToActiveCampaign(Message::create([
      'contact_form' => $this->contactForm->id(),
      'field_boolean' => TRUE,
    ]), $this->activecampaignApiAccount));

    $this->contactFormManager->setEnabled($this->contactForm, $this->activecampaignApiAccount, TRUE);
    $this->contactFormManager->setSendWhen($this->contactForm, $this->activecampaignApiAccount, 'always');
    $this->assertTrue($this->contactFormManager->shouldSendToActiveCampaign(Message::create([
      'contact_form' => $this->contactForm->id(),
      'field_boolean' => FALSE,
    ]), $this->activecampaignApiAccount));
    $this->assertTrue($this->contactFormManager->shouldSendToActiveCampaign(Message::create([
      'contact_form' => $this->contactForm->id(),
      'field_boolean' => TRUE,
    ]), $this->activecampaignApiAccount));

    $this->contactFormManager->setSendWhen($this->contactForm, $this->activecampaignApiAccount, 'checkbox_checked:field_boolean');
    $this->assertFalse($this->contactFormManager->shouldSendToActiveCampaign(Message::create([
      'contact_form' => $this->contactForm->id(),
      'field_boolean' => FALSE,
    ]), $this->activecampaignApiAccount));
    $this->assertTrue($this->contactFormManager->shouldSendToActiveCampaign(Message::create([
      'contact_form' => $this->contactForm->id(),
      'field_boolean' => TRUE,
    ]), $this->activecampaignApiAccount));

  }

}
